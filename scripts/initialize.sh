#!/usr/bin/env bash

set -e

TLMGR_REPOSITORY=http://ftp.jaist.ac.jp/pub/CTAN/systems/texlive/tlnet

echo "TeX Live update (require password)"
sudo tlmgr option repository $TLMGR_REPOSITORY
sudo tlmgr update --self --all --no-persistent-downloads

MACTEX_SETTLED="$HOME/.mactex_settled"

if [ ! -e "$MACTEX_SETTLED" ]; then
  sudo tlmgr repository add http://contrib.texlive.info/current tlcontrib
  sudo tlmgr pinning add tlcontrib '*'
  sudo tlmgr install japanese-otf-nonfree japanese-otf-uptex-nonfree ptex-fontmaps-macos cjk-gs-integrate-macos

  sudo cjk-gs-integrate --link-texmf --cleanup
  sudo cjk-gs-integrate-macos --link-texmf

  sudo mktexlsr

  sudo kanji-config-updmap-sys --jis2004 hiragino-highsierra-pron
  touch $MACTEX_SETTLED
fi

RBENV_VERSION=2.7.2

now_rbenv=$(rbenv version | awk '{print $1}')
if [ "X${now_rbenv}" != "X${RBENV_VERSION}" ]; then
  rbenv install $RBENV_VERSION
  rbenv global $RBENV_VERSION
fi

