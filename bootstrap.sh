#!/usr/bin/env bash

# エラーがあったらそこで即終了、設定していない変数を使ったらエラーにする
set -eu

##### proxy が必要だった時代は /etc/sudoers に env_keep が必要だった。今はいらないのでコメント
# echo "check env_keep(http_proxy, https_proxy) in /etc/sudoers (require password)"
# ENV_KEEP=$(sudo grep http_proxy /etc/sudoers)
# if [ "X${ENV_KEEP}" = "X" ]; then
#   echo '======'
#   echo 'Please add the following two lines in /etc/sudoers using "sudo visudo"'
#   echo 'Defaults    env_keep += "http_proxy"'
#   echo 'Defaults    env_keep += "https_proxy"'
#   exit
# fi

# is_arm という関数を用意しておく。毎回 uname -m を実行するのは莫迦らしいので、UNAME 環境変数で判断
is_arm() { test "$UNAME" = "arm64"; }

# アーキテクチャ名は UNAME に入れておく
UNAME=`uname -m`

# dotfiles の場所を設定
DOTPATH=$HOME/Dropbox/dotfiles

# このフォルダの取り扱い
if [ ! -d "$DOTPATH" ]; then
  # 初回実行時はリポジトリがないので、clone してくる
  echo "Cloning dotfiles.git ..."
  git clone https://hkob@bitbucket.org/hkob/dotfiles.git "$DOTPATH"
else
  # すでにフォルダがある時はそのことを表示
  echo "$DOTPATH already downloaded."
  # 引数に update を付けた場合には、内部をきれいにして pull してくる
  if [ "X$*" = "Xupdate" ]; then
    # 内部で書き換えたものは廃棄して、リポジトリの最新版に差し替える
    echo "Update dotfiles.git ..."
    cd "$DOTPATH"
    git stash
    git checkout master
    git pull origin master
    echo
  fi
fi

cd "$DOTPATH"

# defaults の設定
if [ "$(defaults read com.apple.screencapture disable-shadow)" != "1" ]; then
  defaults write com.apple.screencapture disable-shadow -boolean true
  killall SystemUIServer
fi

# git が入っていなければ、コマンドラインツールをインストール
xcode-select -p 1>/dev/null || {
  echo "Installing Command line tools ..."
  xcode-select --install
  # その場合、M1 Mac では Rosetta2 もインストールされていないと思われるので、こちらもインストール
  if is_arm; then
    # ソフトウェアアップデートで Rosetta2 をインストール。面倒なのでライセンス確認クリックをスキップ
    echo "Installing Rosetta2 ..."
    /usr/sbin/softwareupdate --install-rosetta --agree-to-license

  fi
  echo "Please exec ./bootstrap.sh again in $DOTPATH after installing command-line-tools and Rosetta2(M1 Mac only)."
  exit 1
}

# Rosetta2 でターミナルを動かしている時には強制終了させる
if [ "$UNAME-$(arch -arm64 uname -m)" = "x86_64-arm64" ]; then
  echo "This script can not exec in Rosetta2 terminal"
  exit 1
fi

# ここにある dotfiles をホームに展開 (.git, .DS_Store は除外。他に除外するものが増えたらここに追記)
echo "Deploying dotfiles ..."
for file in .??*; do
    [[ "$file" = ".git" ]] && continue
    [[ "$file" = ".DS_Store" ]] && continue
    ln -fvns "$DOTPATH/$file" "$HOME/$file"
done

# install homebrew
if ! command -v brew > /dev/null 2>&1; then
  # M1 Mac では /opt/homebrew にネイティブ版をインストール
  if is_arm; then
    echo "Installing homebrew in /opt/homebrew for Arm ..."
    # 以前は対応していなかったので直接展開していたが、Homebrew 3.0 になって Apple Silicon に対応
    # cd /opt
    # sudo mkdir homebrew
    # sudo chown $USER:admin homebrew
    # curl -L https://github.com/Homebrew/brew/tarball/master | tar xz --strip 1 -C homebrew
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

    # M1 の場合には、Rosetta2 版の brew を brew_x86 という名前で実行できるようにしておく
    #cd /usr/local/bin
    #ln -sf /usr/local/Homebrew/bin/brew brew_x86
    # この順番でいくと、PATH は 「/opt/homebrew/bin:/usr/local/bin:それ以外」になることを期待している。これは実行してみないとわからない
  else
    # もう Apple Silicon マシンでは Rosetta2 homebrew はインストールしないことにした
    # Install homebrew in Intel Mac or M1 Mac on Rosetta2
    echo "Installing homebrew in /usr/local for Intel or Rosetta2 ..."
    arch -x86_64 /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  fi
fi

# homebrew, cask and mas
if is_arm; then
  # Rosetta brew は廃止
  #  # すでに存在する場合、/opt/homebrew/bin のパスを取り除く
  # PATH=$(echo -n $PATH | tr ':' '\n' | sed "/\/opt\/homebrew\/bin/d" | tr '\n' ':')
  # echo $PATH

  # echo "brew bundle in Rosetta2 ..."
  # cd rosetta2_brew
  # arch --arch x86_64 brew bundle

  # /opt/homebrew/bin のパスを追加
  # PATH=/opt/homebrew/bin:$PATH
  # echo $PATH
  echo "brew bundle in Arm native ..."
  cd arm_brew
  brew bundle -v
  cd ..

  # iTerm2 のシェルとして /usr/local/bin/fish と書きたいため
  ln -sf /opt/homebrew/bin/fish /usr/local/bin/fish

else
  echo "brew bundle in Intel ..."
  cd intel_brew
  brew bundle -v
  cd ..
fi
echo

# Install Rust
if ! command -v rustup > /dev/null 2>&1; then
  rustup-init
fi
rustup update

### mas はプロキシが設定されていると動かなかったので、特殊処理をしていた。今は必要ないのでこの処理を削除
# cd mas_only
# http_proxy_back=$http_proxy
# https_proxy_back=$https_proxy
# unset http_proxy
# unset https_proxy
# brew bundle
# export http_proxy=$http_proxy_back
# export https_proxy=$https_proxy_back
# unset http_proxy_back
# unset https_proxy_back
# cd ..
# echo

# MacTeX を Universal binary に差し替え
#   See: https://oku.edu.mie-u.ac.jp/~okumura/macosx/m1.html
# MacTeX 2021 で必要なくなったのでこの処理を削除
# if is_arm; then
#   if [ ! -f "/etc/paths.d/M1TeX" ]; then
#     echo "Replace Intel TeX binaries to universal ones ..."
#     curl -o MacTeX-2020-Universal.pkg http://www.tug.org/mactex/MacTeX-2020-Universal.pkg
#     pkgutil --expand MacTeX-2020-Universal.pkg /tmp/hoge
#     pax -rz -f /tmp/hoge/MacTeX-2020-Universal-Start.pkg/Payload
#     sudo mv usr/local/texlive/2020/bin/custom /usr/local/texlive/2020/bin/
#     rm -rf usr /tmp/hoge MacTeX-2020-Universal.pkg
#     sudo rm -f /etc/paths.d/TeX
#     echo /usr/local/texlive/2020/bin/custom > M1TeX
#     sudo mv M1TeX /etc/paths.d
#     echo
#     echo "Please exec ./bootstrap.sh again in $DOTPATH after opening new terminal."
#     exit 1
#   fi
# fi

# MacTeX (TeX Live) のアップデート
echo "Updating TeX Live library ..."
TLMGR_REPOSITORY=http://ftp.jaist.ac.jp/pub/CTAN/systems/texlive/tlnet

### Big Sur の特別対応も削除
# TLUTILS_PM=/usr/local/texlive/2020/tlpkg/TeXLive/TLUtils.pm
# TLUTILS_PM_ORIG=${TLUTILS_PM}.orig

# if [ ! -f $TLUTILS_PM_ORIG ]; then
#   echo "Replace $TLUTILS_PM for Big Sur"
#   curl -o TLUtils.pm https://tug.org/svn/texlive/trunk/Master/tlpkg/TeXLive/TLUtils.pm?view=co
#   sudo mv $TLUTILS_PM $TLUTILS_PM_ORIG
#   sudo mv TLUtils.pm $TLUTILS_PM
#   echo
# fi

sudo tlmgr option repository $TLMGR_REPOSITORY
sudo tlmgr update --self --all --no-persistent-downloads

# config の共有の復元
echo "Restoring config files by mackup ..."
mackup restore
echo

# Ruby のアップデート
RBENV_VERSION=3.3.4
RUBY_CONFIGURE_OPTS="--with-openssl-dir=$(brew --prefix openssl@3)"

now_rbenv=$(rbenv version | awk '{print $1}')
if [ "X${now_rbenv}" != "X${RBENV_VERSION}" ]; then
  echo "Installing ruby ..."
  rbenv install $RBENV_VERSION
  rbenv global $RBENV_VERSION
fi

echo "Bootstrapping DONE!"
