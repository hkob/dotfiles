" fish 対策
if $SHELL =~ 'fish'
  set shell=/bin/sh
endif

" プラグインが実際にインストールされるディレクトリ
let s:dein_dir = expand('~/.cache/dein')
" dein.vim 本体
let s:dein_repo_dir = s:dein_dir . '/repos/github.com/Shougo/dein.vim'

" dein.vim がなければ github から落としてくる
if &runtimepath !~# '/dein.vim'
  if !isdirectory(s:dein_repo_dir)
    execute '!git clone https://github.com/Shougo/dein.vim' s:dein_repo_dir
  endif
  execute 'set runtimepath^=' . fnamemodify(s:dein_repo_dir, ':p')
endif

" 設定開始
if dein#load_state(s:dein_dir)
  call dein#begin(s:dein_dir)

  " プラグインリストを収めた TOML ファイル
  " 予め TOML ファイル（後述）を用意しておく
  let g:rc_dir    = expand('~/.vim/rc')
  let s:toml      = g:rc_dir . '/dein.toml'
  let s:lazy_toml = g:rc_dir . '/dein_lazy.toml'

  " TOML を読み込み、キャッシュしておく
  call dein#load_toml(s:toml,      {'lazy': 0})
  call dein#load_toml(s:lazy_toml, {'lazy': 1})

  " 設定終了
  call dein#end()
  call dein#save_state()
endif

" もし、未インストールものものがあったらインストール
if dein#check_install()
  call dein#install()
endif

""""""" その他の設定 """""""

" mapleader の設定
let mapleader = ","
" , の機能は \ で置き換え
noremap \ ,

" FileType

autocmd BufNewFile,BufRead *.axlsx  set filetype=ruby
au FileType html set ts=1 sw=1 expandtab
au FileType ruby set ts=2 sw=2 expandtab
au FileType vim set ts=2 sw=2 expandtab
au FileType sh set ts=2 sw=2 expandtab
au FileType h set ts=4 sw=4 expandtab
au FileType c set ts=4 sw=4 expandtab
au FileType cucumber set ts=2 sw=2 expandtab
au FileType yaml set ts=2 sw=2 expandtab
au FileType haml set ts=2 sw=2 expandtab
au FileType tex set ts=1 sw=1 expandtab
au FileType plantuml set ts=2 sw=2 expandtab
au FileType matlab set ts=4 sw=4 expandtab

" backspace の挙動を決める
set backspace=indent,eol,start

set autoindent         " always set autoindenting on
if has("vms")
	set nobackup          " do not keep a backup file, use versions instead
else
	set backup            " keep a backup file
endif
set history=50          " keep 50 lines of command line history
set ruler               " show the cursor position all the time
set showcmd             " display incomplete commands
"set incsearch          " do incremental searching

filetype plugin indent on     " required!
filetype indent on
syntax on

set t_Co=256
let g:solarized_termcolors=256
"set bg=dark
colorscheme default
"colorscheme solarized
"colorscheme elflord

" コメントを続けない
au FileType * setlocal formatoptions-=ro

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  if has("syntax")
    syntax on
  endif
  set hlsearch
  if &term == "kon" || &term == "linux" || &term == "jfbterm"
    set bg=dark
  endif
endif

let g:tex_conceal = ''
let g:tex_flavor='latex'

nnoremap [unite] <Nop>
nmap <Leader>u [unite]


" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
	if has("syntax")
		syntax on
	endif
	set hlsearch
	if &term == "kon" || &term == "linux" || &term == "jfbterm"
		set bg=dark
	endif
endif
